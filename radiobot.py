import os

from discord import FFmpegPCMAudio
from discord.ext.commands import Bot
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
PREFIX = os.getenv('DISCORD_PREFIX')

client = Bot(command_prefix=list(PREFIX), help_command=None)
version = '0.0.3'

# TODO: Store strings somewhere else
@client.event
async def on_ready():
    print('Music Bot Ready')

@client.command(aliases=['p'])
async def play(ctx, url: str = 'https://tyemyy.fi/stream/dash/tyemyy.mpd'):
    channel = ctx.message.author.voice.channel
    player = await channel.connect()
    player.play(FFmpegPCMAudio(url))
    await ctx.message.channel.send('Now playing, hopefully. Please give me a while to stabilise.')

@client.command(aliases=['s'])
async def stop(ctx):
     await ctx.message.channel.send('Thanks & goodbye & see you later')
     await ctx.voice_client.disconnect()

@client.command(aliases=['h'])
async def help(ctx):
    # sorry for this one
    await ctx.message.channel.send('''Hello! I am a bot. I can play audio web streams in a voice channel\nCommands:\n**!play** to play from the TYEMYY stream. You can also specify some other URL to stream from, e.g. !play https://streamer-uk.rinse.fm:8443/stream to stream from rinse.fm. You must be in a voice channel to give this command. I will join the same one as you automatically.\n**!stop** to stop.\n<@121774297525125120> made me. Contact him if you need to know how to set up the stream, or if something explodes.''')

client.run(TOKEN)
