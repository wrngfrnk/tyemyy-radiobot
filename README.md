A little Discord bot that will broadcast ongoing stream audio from TYEMYY's stream server (or any other web audio stream) to a discord voice channel. No more, no less.

!play [url] to begin. url defaults to the TYEMYY stream server. E.g. **!play https://streamer-uk.rinse.fm:8443/stream** to stream from rinse.fm.

!stop to stop. 

Ask Frank if you want to know how to stream something.
